package com.esprit.examen.services;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.esprit.examen.entities.Formateur;
import com.esprit.examen.entities.Session;
import com.esprit.examen.repositories.FormateurRepository;
import com.esprit.examen.repositories.SessionRepository;


import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;


@Service
public class SessionService implements ISessionService {

	Logger logger = LoggerFactory.getLogger(SessionService.class);

	@Autowired
	SessionRepository sessionRepository;
	@Autowired
	FormateurRepository formateurRepository;

	@Override
	public Long addSession(Session session) {
		sessionRepository.save(session);
		return session.getId();
	}

	@Override
	public List<Session> listSession() {
		return (List<Session>)sessionRepository.findAll();
	}

	@Override
	public Long modifierSession(Session session) {
		sessionRepository.save(session);
		logger.info("Session modifiée avec success");
		return session.getId();
		
	}

	@Override
	public void supprimerSession(Long sessionId) {
		sessionRepository.deleteById(sessionId);
	}

	@Override
	public void affecterFormateurASession(Long formateurId, Long sessionId) throws Exception {

		Session s = sessionRepository.findById(sessionId).orElseThrow(Exception::new);

		Formateur f = formateurRepository.findById(formateurId).orElseThrow(Exception::new);

		if (f.getSessions() == null) {
			Set<Session> sessions = new HashSet<>();
			sessions.add(s);
			f.setSessions(sessions);
		} else {
			f.getSessions().add(s);
		}

		formateurRepository.save(f);
		s.setFormateur(f);
		modifierSession(s);

		logger.info("Affected Succesfully");
	}

	@Override
	public void exportPdfSession() throws JRException, FileNotFoundException {
//

		FileInputStream inputStream = new FileInputStream(
				"C:\\Users\\ASUS\\git\\repository\\GesFormation\\src\\main\\resources\\Jasperizer\\jasp.jrxml");
		JasperReport jasperReport = JasperCompileManager.compileReport(inputStream);
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("name", "wael");
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, param);
		JasperExportManager.exportReportToPdfFile(jasperPrint, "invoice.pdf");

	}

}
