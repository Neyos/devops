package com.esprit.examen.services;

import java.io.FileNotFoundException;
import java.util.List;

import com.esprit.examen.entities.Session;

import net.sf.jasperreports.engine.JRException;

public interface ISessionService {
	Long addSession(Session session);

	Long modifierSession(Session session);
	List<Session> listSession();

	void supprimerSession(Long sessionId);
	void exportPdfSession() throws JRException, FileNotFoundException;
	void affecterFormateurASession (Long formateurId, Long sessionId) throws Exception;
}
