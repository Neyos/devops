package com.esprit.examen.repositories;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.esprit.examen.entities.Contrat;
import com.esprit.examen.entities.Session;

@Repository
public interface SessionRepository extends JpaRepository<Session, Long>{



@Query(nativeQuery=true,value="SELECT test.formateur.nom,\r\n" + 
		"	test.formateur.prenom,\r\n" + 
		"	test.session.date_debut,\r\n" + 
		"	test.session.duree,\r\n" + 
		"	test.session.date_fin,\r\n" + 
		"	test.session.description\r\n" + 
		"FROM test.session\r\n" + 
		"	INNER JOIN test.formateur ON \r\n" + 
		"	 test.session.formateur_id = test.formateur.id")
public List<Object> getListJoint();

}
