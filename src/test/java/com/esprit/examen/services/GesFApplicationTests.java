package com.esprit.examen.services;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.CoreMatchers.any;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import javax.persistence.EntityManager;

import org.assertj.core.api.Assertions;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Example;

import com.esprit.examen.entities.Formateur;
import com.esprit.examen.entities.Session;
import com.esprit.examen.repositories.FormateurRepository;
import com.esprit.examen.repositories.SessionRepository;
import com.esprit.examen.services.FormateurService;
import com.esprit.examen.services.SessionService;



@SpringBootTest
class GesFApplicationTests {

	@Mock

	private EntityManager em = Mockito.mock(EntityManager.class);
	@Autowired
	private SessionRepository sessionRepo;
	@Autowired
	private SessionService myservice;
	@Autowired
	private FormateurRepository formateurRepo;
	@Autowired
	private FormateurService formateurSv;

	@BeforeEach
	public void suppressionDesRessources() {
		formateurRepo.deleteAll();
		sessionRepo.deleteAll();
	}

	@AfterEach
	public void supressionDesRessourcesApres() {
		formateurRepo.deleteAll();
		sessionRepo.deleteAll();
	}

	@Test
	@DisplayName("Mock Test Ajouter Session")
	void ajouterSession() {

		doAnswer(new Answer<Object>() {
			@Override
			public Void answer(InvocationOnMock invocation) {
				Session session = (Session) invocation.getArguments()[0];
				session.setId(1L);
				em.persist(session);

				return null;
			}

		}).when(em).persist(any(Session.class));

	}

	@Test
	@DisplayName("Suppression de session Test")
	void supprimerSession() throws Exception {

		final Session entity = new Session();
		entity.setId(1L);
		myservice.addSession(entity);

		List<Session> resultat = sessionRepo.findAll();
		Long exactId = resultat.get(0).getId();

		if (resultat.get(0).getId() == exactId) {
			sessionRepo.delete(resultat.get(0));
		}
		List<Session> tousLesSession = sessionRepo.findAll();

		if (!tousLesSession.isEmpty())
			fail("la méthode est invoquée incorrectement");

	}

	@Test
	@DisplayName("Affectation Du Session au Formateur Test")
	void affecterSessionFormateur() throws Exception {

		Session session = new Session();
		session.setId(2L);
		myservice.addSession(session);

		Formateur formateur = new Formateur();
		formateur.setId(5L);
		formateurSv.addFormateur(formateur);

		List<Session> Sessions = sessionRepo.findAll();

		List<Formateur> f = formateurRepo.findAll();

		if (f.get(0).getSessions() == null) {
			Set<Session> Assignedsessions = new HashSet<>();
			Assignedsessions.add(session);
			f.get(0).setSessions(Assignedsessions);
		} else {
			f.get(0).getSessions().add(Sessions.get(0));
		}

		formateurSv.addFormateur(f.get(0));
		Sessions.get(0).setFormateur(f.get(0));
		myservice.modifierSession(Sessions.get(0));

		if (f.get(0).getSessions().isEmpty())
			fail("Affectation non effectuée");

	}

	@Test
	@DisplayName("Modification des sessions")
	 void modifierSession() {
		Session session = new Session();
		session.setId(10L);
		sessionRepo.save(session);

		session.setId(25L);
		myservice.modifierSession(session);
		Long exactId = session.getId();
        
		List<Session> sessions = sessionRepo.findAll();
		
		if(sessions.get(0).getId()!=exactId)
			assertThat("Modified successfully");

	}

}
