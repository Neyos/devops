package com.esprit.examen.services;
import static org.junit.jupiter.api.Assertions.*;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import com.esprit.examen.entities.Contrat;
import com.esprit.examen.entities.Formateur;
import com.esprit.examen.entities.Poste;
import com.esprit.examen.entities.TypeCours;
import com.esprit.examen.repositories.FormateurRepository;

@ExtendWith(MockitoExtension.class)
class FormateurServiceTest {
    private static final Logger l = LogManager.getLogger(FormateurServiceTest.class);

    @Mock
    private FormateurRepository formateurRepository;
    @InjectMocks
    private FormateurService underTest;

    @Test
    void testAddFormateur() {
        try {
            l.info("Testing addFormateur()");
            Long a= 5L;
            Formateur f = new Formateur(a,"ahmed","Salhi",Poste.Docteur, Contrat.CDI,"test@test.com","khalil.1");
            assertEquals(Poste.Docteur, f.getPoste());
        } catch (Exception e) {

                  l.error("Erreur dans addFormateur() : " + e);


        }
    }

    @Test
    void modifierFormateur() {
    }

    @Test
    @DisplayName("Multiply two numbers")
    void supprimerFormateur() {

    }

    @Test
    void listFormateurs() {
    }

    @Test
    void nombreFormateursImpliquesDansUnCours() {
    }

    @Nested
    class WhenAddingFormateur {
        @Mock
        private Formateur formateur;

        @BeforeEach
        void setup() {
        }
    }

    @Nested
    class WhenModifieringFormateur {
        @Mock
        private Formateur formateur;

        @BeforeEach
        void setup() {
        }
    }

    @Nested
    class WhenSupprimeringFormateur {
        @BeforeEach
        void setup() {
        }
    }

    @Nested
    class WhenListingFormateurs {
        @BeforeEach
        void setup() {
        }
    }

    @Nested
    class WhenNombringFormateursImpliquesDansUnCours {
        private final TypeCours TYPE_COURS = TypeCours.Informatique;

        @BeforeEach
        void setup() {
        }
    }
}