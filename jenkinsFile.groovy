pipeline {
    agent any
    
    environment {
        EMAIL_TO = 'wael.monsri@espri.tn'
    }
    
    stages {
        stage('GIT') {
            steps {
                echo 'Getting Project'
                git url: 'https://github.com/Neyos/devops', branch: 'main'
            }
        }
        stage('MVN CLEAN') {
            steps {
                echo 'Cleaning Project'
                bat 'C:/Users/wmonsri/Desktop/sts-bundle/apache-maven-3.8.1/bin/mvn clean'
            }
        }
        stage('MVN COMPILE') {
            steps {
                echo 'Compiling Project'
                bat 'C:/Users/wmonsri/Desktop/sts-bundle/apache-maven-3.8.1/bin/mvn compile'
            }
        }
        stage('SONAR') {
            steps {
                echo 'Running project with Sonar'
                bat 'C:/Users/wmonsri/Desktop/sts-bundle/apache-maven-3.8.1/bin/mvn sonar:sonar -Dsonar.host.url=http://localhost:9000 -Dsonar.login=ca36104f72483982296c987a414234a3d5e843fa'
            }
        }
        stage('JUNIT TEST') {
            steps {
                echo 'Running JUnit tests'
                bat 'C:/Users/wmonsri/Desktop/sts-bundle/apache-maven-3.8.1/bin/mvn test'
            }
        }
        stage('PACKAGE') {
            steps {
                echo 'Packaging application'
                bat 'C:/Users/wmonsri/Desktop/sts-bundle/apache-maven-3.8.1/bin/mvn package'
            }
        }
        stage('DEPLOY') {
            steps {
                echo 'Deploying...'
                bat 'C:/Users/wmonsri/Desktop/sts-bundle/apache-maven-3.8.1/bin/mvn deploy:deploy-file -DgroupId=com.esprit.examen -DartifactId=GesF -Dversion=4.0 -DgeneratePom=true -Dpackaging=jar -DrepositoryId=deploymentRepo -Durl=http://localhost:8081/repository/maven-releases/ -Dfile=target/GesF-4.0.jar'
            }
        }
    }
}
